﻿namespace GameOfLife
{
    class Rules
    {
        public Rules() { }

        Cell[] innerList;

        /* GameRules() takes in a copy of the game template called
         * inputCells, we then loop through the inputCells where we 
         * apply the game rules to each cell depending whether it
         * is alive or dead. */
        public Cell[][] GameRules(Cell[][] inputCells)
        {
            var height = inputCells.GetLength(0);
            var width = inputCells[0].Length;

            Cell[][] output = new Cell[height][];
            Cell currCell;
            for (int i = 0; i < inputCells.Length; i++)
            {
                innerList = new Cell[width];
                for (int j = 0; j < inputCells[i].Length; j++)
                {
                    currCell = inputCells[i][j];

                    if (currCell == Cell.Alive)
                    {
                        // Apply the rules on live cells.
                        LiveCellRules(inputCells, i, j);
                    }
                    if (currCell == Cell.Dead)
                    {
                        // Apply the rules on dead cells.
                        DeadCellRules(inputCells, i, j);
                    }
                }
                output[i] = innerList;
            }
            return output;
        }



        /* LiveCellRules() applies the game rules to each Alive cell 
         * in the inputCells 2d array. */
        public Cell[] LiveCellRules(Cell[][] inputCells, int i, int j)
        {
            var numOfLiveNeighbours = 0;

            // Check neighbouring cells of the first cell on the first row.
            if (i == 0 && j == 0)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of last cell on the first row.
            if (i == 0 && j == inputCells[i].Length - 1)
            {
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of the first cell thats not on either
            // the top row or the bottom row.
            if (i != 0 && i != inputCells.Length - 1 && j == 0)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of the last cell thats not on either
            // the top row or the bottom row.
            if (i != 0 && i != inputCells.Length - 1 && j == inputCells[i].Length - 1)
            {
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of any cell thats not at the start or
            // the end of a row or column.
            if (i != 0 && j != 0 && i != inputCells.Length - 1 && j != inputCells[i].Length - 1)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of the first cell on the last row.
            if (i == inputCells.Length - 1 && j == 0)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of the last cell on the last row.
            if (i == inputCells.Length - 1 && j == inputCells[i].Length - 1)
            {
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of any cell on the first row
            // thats not at the start or the end of the row.
            if (i == 0 && j != 0 && j != inputCells[i].Length - 1)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            // Check neighbouring cells of any cell on the last row
            // thats not at the start or the end of the row.
            if (i == inputCells.Length - 1 && j != 0 && j != inputCells[i].Length - 1)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply first game rule.
                if (numOfLiveNeighbours < 2)
                {
                    innerList[j] = Cell.Dead;
                }
                // Apply second game rule.
                if (numOfLiveNeighbours == 2 || numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                // Apply third game rule.
                if (numOfLiveNeighbours > 3)
                {
                    innerList[j] = Cell.Dead;
                }
            }
            return innerList;
        }









        /* DeadCellRules() applies the game rules to each Dead cell 
         * in the inputCells 2d array. */
        public Cell[] DeadCellRules(Cell[][] inputCells, int i, int j)
        {
            var numOfLiveNeighbours = 0;

            // Check neighbouring cells of the first cell on the first row.
            if (i == 0 && j == 0)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of last cell on the first row.
            if (i == 0 && j == inputCells[i].Length - 1)
            {
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of the first cell thats not on either
            // the top row or the bottom row.
            if (i != 0 && i != inputCells.Length - 1 && j == 0)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of the last cell thats not on either
            // the top row or the bottom row.
            if (i != 0 && i != inputCells.Length - 1 && j == inputCells[i].Length - 1)
            {
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of any cell thats not at the start or
            // the end of a row or column.
            if (i != 0 && j != 0 && i != inputCells.Length - 1 && j != inputCells[i].Length - 1)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of the first cell on the last row.
            if (i == inputCells.Length - 1 && j == 0)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of the last cell on the last row.
            if (i == inputCells.Length - 1 && j == inputCells[i].Length - 1)
            {
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of any cell on the first row
            // thats not at the start or the end of the row.
            if (i == 0 && j != 0 && j != inputCells[i].Length - 1)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south eastern neighbour.
                if (inputCells[i + 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check southern neighbour.
                if (inputCells[i + 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check south western neighbour.
                if (inputCells[i + 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            // Check neighbouring cells of any cell on the last row
            // thats not at the start or the end of the row.
            if (i == inputCells.Length - 1 && j != 0 && j != inputCells[i].Length - 1)
            {
                // Check eastern neighbour.
                if (inputCells[i][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check western neighbour.
                if (inputCells[i][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north western neighbour.
                if (inputCells[i - 1][j - 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check northern neighbour.
                if (inputCells[i - 1][j] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Check north eastern neighbour.
                if (inputCells[i - 1][j + 1] == Cell.Alive)
                {
                    numOfLiveNeighbours++;
                }
                // Apply fourth game rule.
                if (numOfLiveNeighbours == 3)
                {
                    innerList[j] = Cell.Alive;
                }
                else
                    innerList[j] = Cell.Dead;
            }
            return innerList;
        }
    }
}
