﻿using System;

namespace GameOfLife
{
    class Menu
    {
        public Menu() { }



        /* ShowMainMenu() displays the main menu options to the user. */
        public void ShowMainMenu()
        {
            Console.WriteLine("\n--- Game of Life ---");
            Console.WriteLine("1. Create Template");
            Console.WriteLine("2. Play Game");
            Console.WriteLine("3. Exit\n");
        }



        /* ShowGameMenu() displays the game menu options to the user. */
        public void ShowGameMenu()
        {
            Console.WriteLine("\n--- Play Game ---");
            Console.WriteLine("1. New Game");
            Console.WriteLine("2. Resume Game\n");
        }



        /* GetMainMenuInput() reads input from the user. If a number 
         * out of the 1 to 3 range or non numeric number is entered, the 
         * loop continues until the correct input is entered. */
        public int GetMenuOptionInput()
        {
            var input = string.Empty;
            var userInput = 0;
            var isValidNum = true;

            do
            {
                Console.Write("Enter an Option: ");
                input = Console.ReadLine();

                // TryParse converts the string representation of a  
                // number to its 32-bit signed integer equivalent.
                isValidNum = int.TryParse(input, out userInput);

                if (userInput < 1 || userInput > 3)
                {
                    Console.WriteLine("Input must be a numeric value of 1 to 3.\n");
                    isValidNum = false;
                }
            } while (!isValidNum);

            return userInput;
        }
    }
}
