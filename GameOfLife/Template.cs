﻿using System;
using System.Text;
using System.IO;

namespace GameOfLife
{
    public class Template : ITemplate
    {
        public Template() { }
        public Template(string name, int height, int width, Cell[][] cells)
        {
            if (cells.Length != height)
            {
                throw new ArgumentException();
            }
            if (cells.Equals(null))
            {
                throw new ArgumentException();
            }
            if (height == -1 || height == 0)
            {
                throw new ArgumentException();
            }
            if (width == -1 || width == 0)
            {
                throw new ArgumentException();
            }
            Name = name;
            Height = height;
            Width = width;
            Cells = cells;
        }

        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public Cell[][] Cells { get; set; }



        /*  CreateTemplate() gets user input such as file name,
         *  grid height, grid width and makes a function call to
         *  GetTemplateCells() for creating a template object to 
         *  be used as an initial game state.*/
        public Template CreateTemplate()
        {
            var name = string.Empty;

            Console.WriteLine("\n--- Create Template ---\n");
            do
            {
                Console.Write("Enter template name: ");
                name = Console.ReadLine();
                Console.WriteLine();

                if (name.Equals(""))
                {
                    Console.WriteLine("You must enter a name for the template.\n");
                }
            } while (name.Equals(""));

            var heightPrompt = "Enter height: ";
            var widthPrompt = "Enter width: ";

            var height = CreateTemplateInput(heightPrompt);
            var width = CreateTemplateInput(widthPrompt);

            // Function call to create and return template cells.
            Cell[][] cells = CreateCells(height, width);

            // Create a template object.
            Template myTemplate = new Template(name, height, width, cells);
            return myTemplate;
        }



        /* CreateTemplateInput() reads in numeric input from the user and
         * validates it until correct user is entered. */
        public int CreateTemplateInput(string prompt)
        {
            var input = string.Empty;
            var result = 0;
            var isValidInput = true;

            do
            {
                Console.Write(prompt);
                input = Console.ReadLine();
                Console.WriteLine();
                isValidInput = int.TryParse(input, out result);

                if (result < 1 || !isValidInput)
                {
                    Console.WriteLine("Input must be a numeric value of 1 or greater.\n");
                    isValidInput = false;
                }
            } while (!isValidInput);
            return result;
        }



        /* CreateCells() reads user input which is used to 
         * create an initial pattern for a template object.
         * Further validation needs to be completed. */
        public Cell[][] CreateCells(int height, int width)
        {
            Cell[][] cells = new Cell[height][];
            Console.WriteLine("Enter cells ('0' is Alive, 'X' is Dead): ");

            for (int i = 0; i < height; i++)
            {
                var row = ReadCellInput(width);
                Console.WriteLine();

                Cell[] innerCells = new Cell[width];
                for (int j = 0; j < row.Length; j++)
                {
                    // Assign correspoonding enums to innerCells array.
                    if (row[j].Equals('o') || row[j].Equals('O'))
                    {
                        innerCells[j] = Cell.Alive;
                    }
                    if (row[j].Equals('x') || row[j].Equals('X'))
                    {
                        innerCells[j] = Cell.Dead;
                    }
                }
                // Assign the innerCells array to the current cells index.
                cells[i] = innerCells;
            }
            return cells;
        }



        /* ReadCellInput() reads input in as string
         * that can only be as long as the passed in length. 
         * Further validation needs to be completed. */
        public string ReadCellInput(int strLen)
        {
            string str = string.Empty;
            do
            {
                char c = Console.ReadKey().KeyChar;
                str += c;

            } while (str.Length < strLen);
            return str;
        }



        /* WriteTemplateToFile() writes the user template input to file. */
        public void WriteTemplateToFile()
        {
            var path = "../Debug/Templates/" + Name + ".txt";
            var mydir = "../Debug/Templates/";

            // If directory path does not exist, create the path.
            try
            {
                if (!Directory.Exists(mydir))
                {
                    // Try to create the directory.
                    DirectoryInfo directoryInfo = Directory.CreateDirectory(mydir);
                    Console.WriteLine("\nDirectory created.\n");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to create directory.");
            }

            // Write the file to our path and deaalocate any resources when finished.
            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                // Use this class' ToString() to write to file.
                // TrimEnd('\n','\r') gets rid of the extra line
                // at the end of the file.
                streamWriter.WriteLine(ToString().TrimEnd('\n', '\r'));
            }
            Console.WriteLine("\nTemplate created.\n");
        }



        /* ToString() prints the the Height, Width and Cells. */
        public override string ToString()
        {
            var cells = BuildCellString();
            return Height + Environment.NewLine + Width + Environment.NewLine + cells;
        }



        /*  BuildCellString() builds a string represenation 
         *  of the Cells 2d array of Dead and Alive cells which
         *  gets called and returned from the ToString() method. */
        public string BuildCellString()
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < Cells.Length; i++)
            {
                for (int j = 0; j < Cells[i].Length; j++)
                {
                    if (Cells[i][j].Equals(Cell.Alive))
                    {
                        builder.Append('O');
                    }
                    if (Cells[i][j].Equals(Cell.Dead))
                    {
                        builder.Append('X');
                    }
                }
                builder.Append(Environment.NewLine);
            }
            return builder.ToString();
        }



        /* ListAllTemplates() lists all the available templates
         * in the Templates directory. 
         * Code on lines 236 to 252 by Matthew Bolger. 
         * (Week 2 chat collaborate). Accessed 8th June 2016. */
        public bool ListAllTemplates()
        {
            Console.WriteLine("\n--- New Game ---");
            Console.WriteLine("Templates:");

            // Get the templates from the directory folder
            // and load them into the templates array.
            var templates = Directory.GetFiles("Templates");

            // Check if there are no templates, if not, notify
            // the user and return to main menu.
            if (templates.Length == 0)
            {
                Console.WriteLine("No templates found - create at"
                       + " least one template before playing a game.");
                return false;
            }

            // List the templates.
            for (int i = 0; i < templates.Length; i++)
            {
                var file = new FileInfo(templates[i]);

                Console.WriteLine($"{i + 1}. {file.Name}");
            }
            return true;
        }



        /* DisplayTemplate() displays all the properties
        * of the passed in game template object and returns
        * it to the caller. */
        public Template DisplayTemplate(Template template)
        {
            Console.WriteLine("\nTemplate");
            Console.WriteLine($"Name  : {template.Name}");
            Console.WriteLine($"Height: {template.Height}");
            Console.WriteLine($"Width : {template.Width}\n");

            Cell[][] cells = template.Cells;

            for (int i = 0; i < cells.Length; i++)
            {
                for (int j = 0; j < cells[i].Length; j++)
                {
                    if (cells[i][j] == Cell.Alive)
                    {
                        Console.Write('\u2588');
                    }
                    if (cells[i][j] == Cell.Dead)
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
            return template;
        }
    }
}
