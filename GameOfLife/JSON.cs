﻿using Newtonsoft.Json;
using System.IO;

namespace GameOfLife
{
    class JSON
    {
        public JSON() { }
        public JSON(int height, int width, int xCoord, int yCoord, Cell[][] gameStateCells)
        {
            Height = height;
            Width = width;
            XCoord = xCoord;
            YCoord = yCoord;
            GameStateCells = gameStateCells;
        }

        public int Height { get; set; }
        public int Width { get; set; }
        public int XCoord { get; set; }
        public int YCoord { get; set; }
        public Cell[][] GameStateCells { get; set; }



        /* SaveToJSON() writes our passed in current game state to JSON file. */
        public void SaveToJSON(JSON savedGame)
        {
            File.WriteAllText("GameOfLife.json", JsonConvert.SerializeObject(savedGame, Formatting.Indented));
        }



        /* DeserializeJSON() deserializes our saved game state file back to
         * a JSON object. We then use that object to resume the game
         * from where it left off. */
        public JSON DeserializeJSON()
        {
            JSON jsonObject = JsonConvert.DeserializeObject<JSON>(File.ReadAllText("GameOfLife.json"));
            return jsonObject;
        }
    }
}
